﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MarkerPlacement : MonoBehaviour {

	public int NumTrials = 3;
	public int NumMarkersPerTrial = 7;

	private List<GameObject> TrialContainers;

	private List<List<GameObject>> MarkerObjects;

	private int currentTrial;
	private int currentMarker;

	private Transform MannequinMeshContainer;

	// Use this for initialization
	void Start () {

		if (MannequinMeshContainer == null) {
			MannequinMeshContainer = transform.Find ("Mannequin Mesh");
			if (MannequinMeshContainer == null) {
				Debug.LogError ("could not find mannequin mesh");
			}
		}

		TrialContainers = new List<GameObject> ();
		MarkerObjects = new List<List<GameObject>> ();

		for (int trialIdx = 1; trialIdx <= NumTrials; trialIdx++) {
			Transform trialContainer = transform.Find ("Task 1 Trial " + trialIdx);
			if (trialContainer == null) {
				Debug.LogError ("Could not find container object for trial " + trialIdx);
			} else {
				TrialContainers.Add (trialContainer.gameObject);

				List<GameObject> markerObjectsForThisTrial = new List<GameObject> ();

				for (int markerIdx = 1; markerIdx <= NumMarkersPerTrial; markerIdx++) {
					Transform markerObject = trialContainer.Find ("CircleAnnotation " + markerIdx);

					if (markerObject == null) {
						Debug.LogError ("Could not find marker " + markerIdx + " in trial " + trialIdx);
					} else {
						markerObjectsForThisTrial.Add (markerObject.gameObject);
					}
				}

				MarkerObjects.Add (markerObjectsForThisTrial);
			}
		}
		HideMannequinMesh ();

		ResetExperiment ();
	}

	void HideMannequinMesh() {
		MannequinMeshContainer.gameObject.SetActive (false);
	}

	void ToggleMannequinMesh() {
		MannequinMeshContainer.gameObject.SetActive (!MannequinMeshContainer.gameObject.activeSelf);
	}

	void SetToMarker(int markerNumber) {
		currentMarker = markerNumber;

		List<GameObject> markerObjectsForThisTrial = MarkerObjects [currentTrial - 1];

		for (int markerIdx = 1; markerIdx <= NumMarkersPerTrial; markerIdx++) {
			GameObject markerObject = markerObjectsForThisTrial [markerIdx - 1];
			markerObject.SetActive (markerIdx <= currentMarker);
		}
	}

	void InitTrial(int trialNumber) {
		currentTrial = trialNumber;

		// hide all other trials
		for (int trialIdx = 1; trialIdx <= NumTrials; trialIdx++) {
			GameObject trialContainer = TrialContainers [trialIdx - 1];
			trialContainer.SetActive (trialIdx == currentTrial);
		}

		SetToMarker (0);
	}

	void Advance() {
		int nextMarkerNumber = currentMarker + 1;
		if (nextMarkerNumber > NumMarkersPerTrial) {
			int nextTrialNumber = currentTrial + 1;
			if (nextTrialNumber > NumTrials) {
				ResetExperiment ();
			} else {
				InitTrial (nextTrialNumber);
			}
		} else {
			SetToMarker (nextMarkerNumber);
		}
	}

	void ResetExperiment() {
		

		InitTrial (1);
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp (KeyCode.Space)) {
			Advance ();
		} else if (Input.GetKeyUp (KeyCode.Backspace)) {
			InitTrial (currentTrial);
		} else if (Input.GetKeyUp (KeyCode.Return)) {
			ResetExperiment ();
		} else if (Input.GetKeyUp (KeyCode.M)) {
			ToggleMannequinMesh ();
		}
	}
}
